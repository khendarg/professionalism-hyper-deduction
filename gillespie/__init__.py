from . import config
from . import engine
import sys
import numpy as np

class Simulation(object):

	def load_config(self, fh, section=None):
		obj = config.parse_config(fh, section)

		self.states = obj['states']
		self.populations = np.reshape(self.states, (1, len(self.states)))
		self.statelabels = obj['states.labels']

		if len(self.statelabels) < len(self.states):
			for i in range(len(self.statelabels), len(self.states)):
				self.statelabels.append('State{}'.format(i))

		self.reactions = obj['reactions']
		self.reactionlabels = obj['reactions.labels']

		if len(self.reactionlabels) < len(self.reactions):
			for i in range(len(self.reactionlabels), self.reactions):
				self.reactionlabels.append('Reaction{}'.format(i))

		self.probabilities = obj['probabilities']

		self.tmax = obj['simulation.tmax']

		self.t0 = obj['simulation.t0']
		self.t = obj['simulation.t0']
		self.times = np.zeros(1) + self.t
		self.dt = np.zeros(0)

	def run(self):
		while self.t < self.tmax:
			try: self.step()
			except ConvergenceAchieved: break

	def get_rtot(self):
		contributions = []
		#TODO: Figure out a vectorization to speed this loop up
		for rxnid, reactants in enumerate(self.reactions < 0):
			if 0 in self.populations[-1,:][reactants]: weight = 0
			else: weight = self.probabilities[rxnid] * np.dot(self.populations[-1,:], reactants)
			contributions.append(weight)
		return np.array(contributions)

	def roll_substrate(self, weights):
		if np.sum(weights != 0) == 1: 
			roll = (weights != 0)
		else: 
			if np.sum(weights) == 0: return None
			renorm = weights / np.sum(weights)
			roll = (np.random.multinomial(1, renorm, size=1)[0] > 0)
		return np.arange(len(weights))[roll][0]

	def step(self):
		weights = self.get_rtot()
		rtot = sum(weights)
		if rtot == 0: raise ConvergenceAchieved

		dt = np.random.exponential(1/rtot)
		#self.dt = np.hstack([self.dt, [dt]])
		self.times = np.hstack([self.times, [self.times[-1] + dt]])

		reaction = self.roll_substrate(weights)

		newpops = self.reactions[reaction] + self.populations[-1,:]
		self.populations = np.vstack([self.populations, newpops])

		self.t += dt

	def to_tsv(self, outfh=sys.stdout):
		out = ''
		out += '#Time\t' + '\t'.join(self.statelabels) + '\n'
		for t, row in zip(self.times, self.populations):
			out += '{:05f}'.format(t)
			for c in row: out += '\t{:d}'.format(c)
			out += '\n'
		outfh.write(out)

class ConvergenceAchieved(BaseException): pass
