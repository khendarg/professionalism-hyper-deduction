import sys
import shlex
import numpy as np
import configparser

def guess_section(config):
	if len(config) > 2: raise ValueError('Ambiguous default section')
	else:
		for section in config:
			if section != 'DEFAULT': 
				print('[WARNING]', 'Selected config section {} automatically!'.format(section))
				return section
			elif section == 'DEFAULT': 
				return section
		raise ValueError('Could not find a valid section')

def parse_config(fh, section=None):
	if isinstance(fh, str): fh = open(fh)

	config = configparser.ConfigParser()

	config.read_file(fh)

	obj = {}

	if section is None: section = guess_section(config)
		
	for floatkey in ('simulation.tmax', 'simulation.t0'):
		obj[floatkey] = config.getfloat(section, floatkey)

	for floatlistkey in ('probabilities',):
		obj[floatlistkey] = np.array([float(x) for x in shlex.split(config.get(section, floatlistkey))])

	#for intkey in ('states',):
	#	obj[intkey] = config.getint(section, intkey)

	for intlistkey in ('reactions', 'states'):
		obj[intlistkey] = np.array([int(x) for x in shlex.split(config.get(section, intlistkey))])

	for strlistkey in ('states.labels', 'reactions.labels'):
		obj[strlistkey] = [x for x in shlex.split(config.get(section, strlistkey))]

	assert len(obj['reactions']) == len(obj['probabilities']) * len(obj['states']), 'Reaction matrix must be of size N_RXN x N_STATE'

	obj['reactions'] = np.reshape(obj['reactions'], (len(obj['reactions']) // len(obj['states']), len(obj['states'])))

	return obj
