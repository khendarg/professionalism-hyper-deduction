#!/usr/bin/env python

import sys
import argparse
import matplotlib.pyplot as plt

def parse_tsv(fh):
	firstline = True
	obj = {'labels':[], 'data':[]}
	for l in fh:
		if firstline and l.startswith('#'): 
			firstline = False
			obj['labels'] = l[1:].split()
		else:
			if not obj['data']: 
				for c in l.split(): obj['data'].append([])
			for i, col in enumerate(l.split()):
				obj['data'][i].append(float(col))
	return obj

def main(fh, outfile):
	table = parse_tsv(fh)

	fig = plt.figure()
	ax = fig.gca()

	for label, data in zip(table['labels'][1:], table['data'][1:]):
		ax.plot(table['data'][0], data, label=label)

	ax.legend()

	if outfile: fig.savefig(outfile)
	else: plt.show()
				

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Small TSV plotter for run_gillespie.py output')

	parser.add_argument('infile', type=argparse.FileType('r'), nargs='?', default=sys.stdin, help='Input TSV (which need not even be run_gillespie.py output)')

	parser.add_argument('-o', help='Write plot to file')
	args = parser.parse_args()

	main(args.infile, args.o)

