#!/usr/bin/env python

import sys
import argparse
import gillespie


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='A light wrapper for running typical Gillespie algorithm tasks')

	parser.add_argument('infile', type=argparse.FileType('r'), help='CFG file with simulation parameters')
	parser.add_argument('--section', default='DEFAULT', help='CFG section to load (if using multi-section CFGs')
	parser.add_argument('-o', type=argparse.FileType('w'), default=sys.stdout, help='Where to save the resulting data (default: stdout)')

	args = parser.parse_args()

	sim = gillespie.Simulation()
	sim.load_config(args.infile, section=args.section)
	sim.run()
	sim.to_tsv(args.o)
