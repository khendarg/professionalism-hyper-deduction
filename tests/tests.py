#!/usr/bin/env python

import unittest
import gillespie

class TestSIRConfig(unittest.TestCase):

	cfgfn = '../demos/sir.cfg'

	def test_read_config(self):
		sim = gillespie.Simulation()
		with open(self.cfgfn) as fh:
			sim.load_config(fh)

	def test_run_sim(self):
		sim = gillespie.Simulation()
		with open(self.cfgfn) as fh:
			sim.load_config(fh)
			sim.run()
	def test_write_tsv(self):
		sim = gillespie.Simulation()
		with open(self.cfgfn) as fh:
			sim.load_config(fh)
			sim.run()
			sim.to_tsv()
			


if __name__ == '__main__':
	unittest.main()
